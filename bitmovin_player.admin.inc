<?php

/**
 * @file
 * Bitmovin player admin pages.
 */

/**
 * Build the Bitmovin player admin settings form.
 */
function bitmovin_player_setting_form($form, &$form_state) {
  $form['bitmovin_player_cdn'] = array(
    '#type' => 'textfield',
    '#title' => t('Default CDN URL'),
    '#description' => t('Setup a custom location for bitdash.min.js.'),
    '#default_value' => variable_get('bitmovin_player_cdn', BITMOVIN_PLAYER_CDN_URL),
    '#required' => TRUE,
  );
  $form['bitmovin_player_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Bitmovin API key'),
    '#description' => t('Your Bitmovin API key which can be found !link.', array('!link' => l(t('here'), 'https://app.bitmovin.com/settings'))),
    '#default_value' => variable_get('bitmovin_player_api_key', ''),
    '#required' => TRUE,
  );
  $form['bitmovin_player_player_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Bitmovin player key'),
    '#description' => t('Your player license key which can be found !link.', array('!link' => l(t('here'), 'https://app.bitmovin.com/player/overview'))),
    '#default_value' => variable_get('bitmovin_player_player_key', ''),
    '#required' => TRUE,
  );
  $form['bitmovin_player_skin'] = array(
    '#type' => 'textarea',
    '#title' => t('Bitmovin player skin'),
    '#description' => t('Custom skin for the Bitmovin player. Create a custom skin !link.', array('!link' => l(t('here'), 'https://bitmovin.com/SkinGenerator/index.html'))),
    '#default_value' => variable_get('bitmovin_player_skin', ''),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}

/**
 * Validate handler for the Bitmovin player admin settings form.
 */
function bitmovin_player_setting_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  if (!preg_match('/^(https?)?\/\/.*js$/', $values['bitmovin_player_cdn'])) {
    form_set_error('bitmovin_player_cdn', t('Invalid URL'));
  }
  if (!preg_match('/[a-f0-9]{64}/', $values['bitmovin_player_api_key'])
    || strlen($values['bitmovin_player_api_key']) != 64) {
    form_set_error('bitmovin_player_api_key', t('Invalid API key'));
  }
  if (!preg_match('/[a-f0-9\-]{36}/', $values['bitmovin_player_player_key'])
    || strlen($values['bitmovin_player_player_key']) != 36) {
    form_set_error('bitmovin_player_player_key', t('Invalid player licence key'));
  }
  if (!empty($values['bitmovin_player_skin'])
    && !drupal_json_decode($values['bitmovin_player_skin'])) {
    form_set_error('bitmovin_player_skin', t('Skin is not valid JSON'));
  }
}
