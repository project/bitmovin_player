## Installation with Drush
* Download the module, and composer manager: `drush dl bitmovin_player,
  composer_manager, entity`.
* Enable Composer Manager: `drush en composer_manager`.
* Composer Manager writes a file to `sites/default/files/composer`.
* Enable Bitmovin player: `drush en bitmovin_player`.
* As long as Composer Manager is enabled, the required dependencies will be
added to `sites/all/vendor` as soon as you enable the Bitmovin player module.
* If you don't see any dependencies download, try `drush composer-json-rebuild`
 followed by `drush composer-manager install` when in docroot.
* Check `/admin/config/system/composer-manager` to ensure it's all green.
* Tip: If you ever want to update your composer dependencies to a more recent
 version (while respecting versioning constraints) try `drush composer-manager
  update`.
* Tip: See composer manager Drupal documentation to understand how this all
 works.

## Manual installation using composer
Bitmovin Player requires the bitmovin/bitcodin-php library. You have to install
this library using composer.

* Download the [Composer Manager](http://drupal.org/project/composer_manager)
  module to the modules directory.
* Download the [Entity API](http://drupal.org/project/entity) module to the
  modules directory.
* Enable Bitmovin Player at `/admin/modules`.
* Navigate to the bitmovin_player module-directory
  (by default `sites/all/modules/contrib`) using your command line tool.
* Run in the command line `composer install`. The required dependencies will be
added to `sites/all/vendor`.

## Configuration
* Navigate to `/admin/config/media/bitmovin`.
* Fill in your Bitmovin API key.
  The API Key can be found [here](https://app.bitmovin.com/settings).
* Fill in your Bitmovin player key.
  The Player Key can be found [here](https://app.bitmovin.com/player/overview).
* (Optional) Use a custom player skin.
  Create a custom skin [here](https://bitmovin.com/SkinGenerator/index.html).
* Save the form.

## Todo:

* Adding CKEditor plugin for bitmovin.
