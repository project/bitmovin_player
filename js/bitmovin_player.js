/**
 * @file
 * Bitmovin's JavaScript configuration.
 *
 * @TODO: When upgrading to API version 6, make sure to rename the API calls as
 * well.
 */

(function ($) {

  /**
   * Builds and configures the player.
   */
  Drupal.behaviors.bitmovinPlayer = {
    attach: function (context, settings) {
      var key = {
        key: Drupal.settings.bitmovin_player.player_key,
      };

      // Initialize all players.
      $('.bitmovin-player').once('bitmovin-player').each(function () {
        var $elem = $(this);
        var data = $elem.data();
        var player_id = $elem.attr('id');
        var player = bitdash(player_id);
        var width = Drupal.settings.bitmovin_player.player_width;
        var height = Drupal.settings.bitmovin_player.player_height || false;
        var skin = Drupal.settings.bitmovin_player.skin_location;

        var config = {
          source: {
            dash: data.dash,
            hls: data.hls,
            progressive: data.progressive,
            poster: data.poster
          },
          playback: {
            autoplay: data.autoplay
          },
          style: {
            width: width
          }
        };

        if (height) {
          config.style.height = height;
        }

        var btconf = $.extend(key, config);
        player.setup(btconf).then(function (value) {
          $elem.trigger('player_ready');
        }, function (reason) {
          console.log('Could not play file')
        });

        if (skin !== null) {
          bitdash(player_id).setSkin(skin);
        }

      });

    }
  };

})(jQuery);
