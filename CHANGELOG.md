
bitdash_player 7.x-1.x-dev
-----------------------------

* \#2809417 by SilvanWakker: Class value not an array
* \#2813949 by SilvanWakker: Not the latest API version
* \#2813945 by SilvanWakker: Default CDN value on admin page
* \#2813915 by SilvanWakker: Automatic height
* \#2813931 by SilvanWakker: Add a trigger for use in add-on modules
* Rename project files from "Bitdash Player" to "Bitmovin player"
* Improved some texts to make it more clear as to what it means/does

bitdash_player 7.x-1.0-alpha4
-----------------------------

* \#2802779 by SilvanWakker: Customizable player width/height

bitdash_player 7.x-1.0-alpha3
-----------------------------

* \#2792115 by SilvanWakker: Non-static function called statically: copyDirectory()
* Add CHANGELOG.md
* \#2797081 by SilvanWakker: Host on external FTP
* \#2798745 by SilvanWakker: Custom skins

bitdash_player 7.x-1.0-alpha2
-----------------------------

* Update the README file with a description

bitdash_player 7.x-1.0-alpha1
-----------------------------
